Module: Lazy Counter
Author: Aleksandr Borkov <https://www.drupal.org/sandbox/sbor767/2420645>

Description
===========
This is 7.x-8.x version for Lesson8 considered work with Work with Drupal
 Test framework, based on 7.x-5.x Lesson5.

Count page visits by user with 'page visits counter' permission, and display it
 when user login.

This is my lesson#8 for training by Konstantin Komelin, http://morningcurve.com
 to study the Drupal system.

Installation
============
* Copy the 'lazy_counter' module directory in to your Drupal sites/all/modules
    directory as usual.
* After install, set 'page visits counter' permission
    under group 'Lazy Counter' module to 'AUTHENTICATED USER' roles, and setup
    prize threshold and time range limits on
    admin/config/system/lazy-counter/adjust path.
