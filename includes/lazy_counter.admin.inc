<?php
/**
 * @file Callbacks for admin pages.
 *
 * Date: 12.02.2015
 * Time: 13:50
 */

/**
 * Menu callback; Renders users with score > 0.
 *
 * @return string
 *   Return rendered string with active users by its score.
 */
function lazy_counter_active_users() {
  $output = t('Member List sorted by number of points:');

  $users = db_select('lazy_counter', 'c')
    ->fields('c')
    ->execute()
    ->fetchAllAssoc('uid');

  uasort($users, '_lazy_counter_cmp');

  foreach ($users as $uid => $row) {
    $username = user_load($uid)->name;
    $output .= '<div>' . check_plain($username)
      . ' &rarr; ' . check_plain($row->points) . '</div>';
  }

  return $output;
}

/**
 * Hendler func for sort array $users by 'points'.
 */
function _lazy_counter_cmp(&$a, &$b) {
  if ($a->points == $b->points) {
    return 0;
  }
  return ($a->points < $b->points) ? -1 : 1;
}

/**
 * Menu form callback.
 *
 * @return array
 *
 *   Adjust score reset and trophy conditions.
 */
function lazy_counter_form_adjust($form, &$form_state) {
  $form = array();

  $form['lazy_counter_reset_period'] = array(
    '#type' => 'select',
    '#title' => t('Counter reset period'),
    '#options' => array(
      1 => t('1 month'),
      3 => t('3 month'),
      6 => t('Half year'),
      12 => t('Year'),
    ),
    '#default_value' => variable_get('lazy_counter_reset_period', 1),
    '#description' => t('Set for adjust reset counters period'),
    '#ajax' => array(
      'callback' => 'lazy_counter_form_adjust_callback',
      'wrapper' => 'lazy_counter_form_adjust_wrapper',
      'effect' => 'fade',
    ),
  );

  $form['lazy_counter_prize_level'] = array(
    '#type' => 'textfield',
    '#title' => t('Prize level'),
    '#default_value' => variable_get('lazy_counter_prize_level', 140),
    '#description' => t('Set for adjust prize level'),
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('Enter prize level')),
    '#ajax' => array(
      'callback' => 'lazy_counter_form_adjust_callback',
      'wrapper' => 'lazy_counter_form_adjust_wrapper',
      // 'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['#prefix'] = '<div id="lazy_counter_form_adjust_wrapper">';
  $form['#suffix'] = '</div>';

  return $form;
}

/**
 * Validate handler.
 */
function lazy_counter_form_adjust_validate($form, &$form_state) {

  if (!is_numeric($form_state['values']['lazy_counter_prize_level'])) {
    form_set_error('lazy_counter_prize_level', t('Your must use numeric!'));
  }

}

/**
 * Sabmit handler.
 */
function lazy_counter_form_adjust_submit($form, &$form_state) {

  variable_set('lazy_counter_reset_period', $form_state['values']['lazy_counter_reset_period']);
  variable_set('lazy_counter_prize_level', $form_state['values']['lazy_counter_prize_level']);

}

/**
 * Ajax callback.
 */
function lazy_counter_form_adjust_callback($form, &$form_state) {

  lazy_counter_form_adjust_validate($form, $form_state);
  if (form_get_errors()) {
    return $form;
  }

  lazy_counter_form_adjust_submit($form, $form_state);

  drupal_set_message(t('Your settings saved.'));

  return $form;
}

/**
 * Menu form callback.
 *
 * Allows you to award the selected user.
 */
function lazy_counter_form_promote() {
  $form = array();

  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#description' => t('Type user name'),
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('Enter user name')),
    '#autocomplete_path' => 'lazy_counter_form_promote_callback',
  );

  $form['add_points'] = array(
    '#type' => 'radios',
    '#title' => t('Add points'),
    '#options' => array(
      10 => t('10 points'),
      20 => t('20 points'),
      50 => t('50 points'),
    ),
    '#default_value' => '20',
    '#description' => t('How much points would you like add for this user?'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Validate handler.
 */
function lazy_counter_form_promote_validate($form, &$form_state) {
  $username = $form_state['values']['username'];
  $uid = user_load_by_name($username);

  if (!$uid) {
    form_set_error('username', t('Please enter correct user name'));
  }

}

/**
 * Submit handler.
 */
function lazy_counter_form_promote_submit($form, &$form_value) {

  $username = $form_value['values']['username'];
  $uid = user_load_by_name($username)->uid;
  $add_points = $form_value['values']['add_points'];

  $points = _lazy_counter_get_row($uid)->points + $add_points;
  _lazy_counter_set_row($uid, $points);

  drupal_set_message(
    t('User @user get @points points, and have now @pointsnow points!',
      array(
        '@user' => $username,
        '@points' => $add_points,
        '@pointsnow' => $points,
      )
    )
  );

}

/**
 * Autocomplete callback.
 */
function lazy_counter_form_promote_callback($keyword = NULL) {

  $query = db_select('users', 'u');
  $query->fields('u', array('name'));
  $query->condition('name', '%' . $keyword . '%', 'LIKE');
  $query->range(0, 10);
  $results = $query->execute();

  $names = array();
  while ($user = $results->fetchAssoc()) {
    // Use checkplain() when appropriate.
    $names[$user['name']] = $user['name'];
  }

  drupal_json_output($names);

}
