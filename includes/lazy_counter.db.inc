<?php
/**
 * @file Helpers.
 *
 * Intended for collect helper functions.
 *
 * Date: 12.02.2015
 * Time: 13:50
 */

/**
 * Helper function for getting values from table by uid.
 *
 * @return object.
 *   Return object from table row for selected uid.
 */
function _lazy_counter_get_row($uid) {

  $query = db_select('lazy_counter', 'c');

  $query
    ->fields('c')
    ->condition('uid', $uid);

  $result = $query->execute()->fetchAllAssoc('uid');

  if (array_key_exists($uid, $result)) {
    return $result[$uid];
  }
  else {
    /* Return nulled object for empty table row for selected uid.
    /* Otherwise you get php NOTICE, in case access to result.
    /* Some notes:
     * Only points = 0 in case user been reseted or win prize.
     * Only reset_date = 0 in case user never logged.
     * Both points and reset_date = 0 in case init module.
     */
    $row = new stdClass();
    $row->uid = $uid;
    $row->points = 0;
    $row->reset_date = 0;
    return $row;
  }

}

/**
 * Helper function for set variables to table row by uid.
 */
function _lazy_counter_set_row($uid, $points, $reset_date = NULL) {

  $exist = db_select('lazy_counter', 'c')
    ->fields('c')
    ->condition('uid', $uid)
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();

  /* If no param use old data from db. */
  if ($exist && !$reset_date) {
    $reset_date = $exist['reset_date'];
  }

  $row = array(
    'uid' => $uid,
    'points' => $points,
    'reset_date' => $reset_date,
  );

  if ($exist) {
    /* If exist we need use PK - see docs. */
    drupal_write_record('lazy_counter', $row, 'uid');
  }
  else {
    drupal_write_record('lazy_counter', $row);
  }

}
